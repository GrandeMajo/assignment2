//
// Questo file a stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andra persa durante la ricompilazione dello schema di origine. 
// Generato il: 2016.01.17 alle 11:40:01 PM CET 
//


package it.polito.dp2.WF.sol2.jaxb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.polito.dp2.WF.sol2.jaxb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _WorkflowMonitor_QNAME = new QName("", "workflowMonitor");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.polito.dp2.WF.sol2.jaxb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ActionStatusType }
     * 
     */
    public ActionStatusType createActionStatusType() {
        return new ActionStatusType();
    }

    /**
     * Create an instance of {@link WorkflowMonitorType }
     * 
     */
    public WorkflowMonitorType createWorkflowMonitorType() {
        return new WorkflowMonitorType();
    }

    /**
     * Create an instance of {@link ActionType }
     * 
     */
    public ActionType createActionType() {
        return new ActionType();
    }

    /**
     * Create an instance of {@link ProcessActionType }
     * 
     */
    public ProcessActionType createProcessActionType() {
        return new ProcessActionType();
    }

    /**
     * Create an instance of {@link SimpleActionType }
     * 
     */
    public SimpleActionType createSimpleActionType() {
        return new SimpleActionType();
    }

    /**
     * Create an instance of {@link WorkflowType }
     * 
     */
    public WorkflowType createWorkflowType() {
        return new WorkflowType();
    }

    /**
     * Create an instance of {@link ProcessType }
     * 
     */
    public ProcessType createProcessType() {
        return new ProcessType();
    }

    /**
     * Create an instance of {@link ActionStatusType.Actor }
     * 
     */
    public ActionStatusType.Actor createActionStatusTypeActor() {
        return new ActionStatusType.Actor();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link WorkflowMonitorType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "workflowMonitor")
    public JAXBElement<WorkflowMonitorType> createWorkflowMonitor(WorkflowMonitorType value) {
        return new JAXBElement<WorkflowMonitorType>(_WorkflowMonitor_QNAME, WorkflowMonitorType.class, null, value);
    }

}
