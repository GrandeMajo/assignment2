//
// Questo file a stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andra persa durante la ricompilazione dello schema di origine. 
// Generato il: 2016.01.17 alle 11:40:01 PM CET 
//


package it.polito.dp2.WF.sol2.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Classe Java per actionStatusType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="actionStatusType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="actor" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="name" use="required" type="{}roleType" />
 *                 &lt;attribute name="role" use="required" type="{}roleType" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *       &lt;attribute name="name" use="required" type="{}nameType" />
 *       &lt;attribute name="isTakenInCharge" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="isTerminated" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *       &lt;attribute name="terminationTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actionStatusType", propOrder = {
    "actor"
})
public class ActionStatusType {

    protected ActionStatusType.Actor actor;
    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "isTakenInCharge")
    protected Boolean isTakenInCharge;
    @XmlAttribute(name = "isTerminated")
    protected Boolean isTerminated;
    @XmlAttribute(name = "terminationTime")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar terminationTime;

    /**
     * Recupera il valore della proprieta actor.
     * 
     * @return
     *     possible object is
     *     {@link ActionStatusType.Actor }
     *     
     */
    public ActionStatusType.Actor getActor() {
        return actor;
    }

    /**
     * Imposta il valore della proprieta actor.
     * 
     * @param value
     *     allowed object is
     *     {@link ActionStatusType.Actor }
     *     
     */
    public void setActor(ActionStatusType.Actor value) {
        this.actor = value;
    }

    /**
     * Recupera il valore della proprieta name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il valore della proprieta name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Recupera il valore della proprieta isTakenInCharge.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTakenInCharge() {
        return isTakenInCharge;
    }

    /**
     * Imposta il valore della proprieta isTakenInCharge.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTakenInCharge(Boolean value) {
        this.isTakenInCharge = value;
    }

    /**
     * Recupera il valore della proprieta isTerminated.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsTerminated() {
        return isTerminated;
    }

    /**
     * Imposta il valore della proprieta isTerminated.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsTerminated(Boolean value) {
        this.isTerminated = value;
    }

    /**
     * Recupera il valore della proprieta terminationTime.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTerminationTime() {
        return terminationTime;
    }

    /**
     * Imposta il valore della proprieta terminationTime.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTerminationTime(XMLGregorianCalendar value) {
        this.terminationTime = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="name" use="required" type="{}roleType" />
     *       &lt;attribute name="role" use="required" type="{}roleType" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Actor {

        @XmlAttribute(name = "name", required = true)
        protected String name;
        @XmlAttribute(name = "role", required = true)
        protected String role;

        /**
         * Recupera il valore della proprieta name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Imposta il valore della proprieta name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Recupera il valore della proprieta role.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRole() {
            return role;
        }

        /**
         * Imposta il valore della proprieta role.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRole(String value) {
            this.role = value;
        }

    }

}
