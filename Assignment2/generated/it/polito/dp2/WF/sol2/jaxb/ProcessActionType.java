//
// Questo file a stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andra persa durante la ricompilazione dello schema di origine. 
// Generato il: 2016.01.17 alle 11:40:01 PM CET 
//


package it.polito.dp2.WF.sol2.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per processActionType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="processActionType">
 *   &lt;complexContent>
 *     &lt;extension base="{}actionType">
 *       &lt;attribute name="actionWorkflow" use="required" type="{}nameType" />
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processActionType")
public class ProcessActionType
    extends ActionType
{

    @XmlAttribute(name = "actionWorkflow", required = true)
    protected String actionWorkflow;

    /**
     * Recupera il valore della proprieta actionWorkflow.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getActionWorkflow() {
        return actionWorkflow;
    }

    /**
     * Imposta il valore della proprieta actionWorkflow.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setActionWorkflow(String value) {
        this.actionWorkflow = value;
    }

}
