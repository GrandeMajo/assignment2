//
// Questo file a stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andra persa durante la ricompilazione dello schema di origine. 
// Generato il: 2016.01.17 alle 11:40:01 PM CET 
//


package it.polito.dp2.WF.sol2.jaxb;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per actionType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="actionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="name" use="required" type="{}nameType" />
 *       &lt;attribute name="enclosingWorkflow" use="required" type="{}nameType" />
 *       &lt;attribute name="role" use="required" type="{}roleType" />
 *       &lt;attribute name="isAutomaticallyInstantiated" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "actionType")
@XmlSeeAlso({
    ProcessActionType.class,
    SimpleActionType.class
})
public class ActionType {

    @XmlAttribute(name = "name", required = true)
    protected String name;
    @XmlAttribute(name = "enclosingWorkflow", required = true)
    protected String enclosingWorkflow;
    @XmlAttribute(name = "role", required = true)
    protected String role;
    @XmlAttribute(name = "isAutomaticallyInstantiated")
    protected Boolean isAutomaticallyInstantiated;

    /**
     * Recupera il valore della proprieta name.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Imposta il valore della proprieta name.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Recupera il valore della proprieta enclosingWorkflow.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnclosingWorkflow() {
        return enclosingWorkflow;
    }

    /**
     * Imposta il valore della proprieta enclosingWorkflow.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnclosingWorkflow(String value) {
        this.enclosingWorkflow = value;
    }

    /**
     * Recupera il valore della proprieta role.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRole() {
        return role;
    }

    /**
     * Imposta il valore della proprieta role.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRole(String value) {
        this.role = value;
    }

    /**
     * Recupera il valore della proprieta isAutomaticallyInstantiated.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAutomaticallyInstantiated() {
        return isAutomaticallyInstantiated;
    }

    /**
     * Imposta il valore della proprieta isAutomaticallyInstantiated.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAutomaticallyInstantiated(Boolean value) {
        this.isAutomaticallyInstantiated = value;
    }

}
