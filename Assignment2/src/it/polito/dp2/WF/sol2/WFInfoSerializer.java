package it.polito.dp2.WF.sol2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

import it.polito.dp2.WF.ActionReader;
import it.polito.dp2.WF.ActionStatusReader;
import it.polito.dp2.WF.Actor;
import it.polito.dp2.WF.ProcessActionReader;
import it.polito.dp2.WF.ProcessReader;
import it.polito.dp2.WF.SimpleActionReader;
import it.polito.dp2.WF.WorkflowMonitor;
import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.WorkflowMonitorFactory;
import it.polito.dp2.WF.WorkflowReader;
import it.polito.dp2.WF.sol2.Resources;
import it.polito.dp2.WF.sol2.jaxb.ActionStatusType;
import it.polito.dp2.WF.sol2.jaxb.ObjectFactory;
import it.polito.dp2.WF.sol2.jaxb.ProcessActionType;
import it.polito.dp2.WF.sol2.jaxb.ProcessType;
import it.polito.dp2.WF.sol2.jaxb.SimpleActionType;
import it.polito.dp2.WF.sol2.jaxb.WorkflowMonitorType;
import it.polito.dp2.WF.sol2.jaxb.WorkflowType;
import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class WFInfoSerializer {
	private static Logger logger = Logger.getLogger(WFInfoSerializer.class.getName());
	
	private WorkflowMonitor monitor;

	public WFInfoSerializer() throws WorkflowMonitorException {
		Resources.setLogHandler(logger, true);
		try {
			WorkflowMonitorFactory factory = WorkflowMonitorFactory.newInstance();
			monitor = factory.newWorkflowMonitor();
		} catch (WorkflowMonitorException wme) {
			logger.severe(wme.getMessage());
			throw wme;
		}  catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		}
	}

	public WFInfoSerializer(WorkflowMonitor monitor) {
		Resources.setLogHandler(logger, true);
		this.monitor = monitor;
	}
	
	public void serialize(String outputFileName) throws SAXException, JAXBException, FileNotFoundException {
		ObjectFactory instantiator = new ObjectFactory();
		WorkflowMonitorType workflowMonitorType = instantiator.createWorkflowMonitorType();
		elaborateWorkflowMonitor(instantiator, workflowMonitorType);
		
		SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(new File(Resources.SCHEMA_LOCATION));
		
		JAXBElement<WorkflowMonitorType> element = instantiator.createWorkflowMonitor(workflowMonitorType);
		JAXBContext context = JAXBContext.newInstance(Resources.CONTEXT_PATH);
		
		logger.info("Marshalling content tree root on file " + outputFileName);
		
		Marshaller marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
		marshaller.setProperty(Marshaller.JAXB_NO_NAMESPACE_SCHEMA_LOCATION, Resources.SCHEMA_LOCATION);
		marshaller.setSchema(schema);
		marshaller.marshal(element, new FileOutputStream(outputFileName));
		
		logger.info("Marshalling completed");
	}
	
	public void writeOnFile(String outputFileName) {
		
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			throw new RuntimeException("Invalid command line argument (it must be -Doutput=<fileName>.xml)");
		}

		WFInfoSerializer serializer;
		try {
			serializer = new WFInfoSerializer();
			logger.info("Starting serializer...");
			// create a JAXBContext
			serializer.serialize(args[0]);

		} catch(JAXBException je) {
			logger.severe(je.getMessage());
		} catch (WorkflowMonitorException e) {
			logger.severe(e.getMessage());
		} catch (Exception e) {
			logger.severe(e.getMessage());
		} finally {
			logger.info("Serializer terminated.");			
		}
	}
	
	private void elaborateWorkflowMonitor(ObjectFactory instantiator, WorkflowMonitorType workflowMonitorType) {
		if (workflowMonitorType == null) {
			workflowMonitorType = instantiator.createWorkflowMonitorType();
		}
		
		Set<WorkflowReader> workflowReaders = monitor.getWorkflows();
		List<WorkflowType> workflowTypes = workflowMonitorType.getWorkflow();

		for (WorkflowReader workflowReader : workflowReaders)
			workflowTypes.add(getWorkflowTypeFromWorkflowReader(workflowReader));
		
		Set<ProcessReader> processReaders = monitor.getProcesses();
		List<ProcessType> processTypes = workflowMonitorType.getProcess();
		
		for (ProcessReader processReader : processReaders)
			processTypes.add(getProcessTypeFromProcessReader(processReader));
	}
	
	private WorkflowType getWorkflowTypeFromWorkflowReader(WorkflowReader workflowReader) {
		WorkflowType workflowType = new WorkflowType();
		workflowType.setName(workflowReader.getName());
		
		Set<ActionReader> actionReaders = workflowReader.getActions();
		List<SimpleActionType> simpleActionTypes = workflowType.getSimpleAction();
		List<ProcessActionType> processActionTypes = workflowType.getProcessAction();
		
		for (ActionReader actionReader : actionReaders) {
			if (actionReader instanceof SimpleActionReader) {
				simpleActionTypes.add(getSimpleActionTypeFromSimpleActionReader((SimpleActionReader) actionReader));
			} else if (actionReader instanceof ProcessActionReader) {
				processActionTypes.add(getProcessActionTypeFromProcessActionReader((ProcessActionReader) actionReader));
			} else
				throw new RuntimeException("Invalid istance of ActionReader");
		}
		
		return workflowType;
	}
	
	private ProcessType getProcessTypeFromProcessReader(ProcessReader processReader) {
		ProcessType processType = new ProcessType();
		processType.setWorkflow(processReader.getWorkflow().getName());
		processType.setStartTime(Resources.getXMLGregorianCalendar((GregorianCalendar) processReader.getStartTime()));
		
		List<ActionStatusReader> actionStatusReaders = processReader.getStatus();
		List<ActionStatusType> actionStatusTypes = processType.getActionStatus();
		
		for (ActionStatusReader actionStatusReader : actionStatusReaders)
			actionStatusTypes.add(getActionStatusTypeFromActionStatusReader(actionStatusReader));
		
		return processType;
	}
	
	private SimpleActionType getSimpleActionTypeFromSimpleActionReader(SimpleActionReader simpleActionReader) {
		SimpleActionType simpleActionType = new SimpleActionType();
		simpleActionType.setName(simpleActionReader.getName());
		simpleActionType.setEnclosingWorkflow(simpleActionReader.getEnclosingWorkflow().getName());
		simpleActionType.setRole(simpleActionReader.getRole());
		simpleActionType.setIsAutomaticallyInstantiated(simpleActionReader.isAutomaticallyInstantiated());
		
		Set<ActionReader> nextActions = simpleActionReader.getPossibleNextActions();
		List<String> nextActionNames = simpleActionType.getNextAction();
		
		for (ActionReader nextAction : nextActions)
			nextActionNames.add(nextAction.getName());
		
		return simpleActionType;
	}
	
	private ProcessActionType getProcessActionTypeFromProcessActionReader(ProcessActionReader processActionReader) {
		ProcessActionType processActionType = new ProcessActionType();
		processActionType.setName(processActionReader.getName());
		processActionType.setEnclosingWorkflow(processActionReader.getEnclosingWorkflow().getName());
		processActionType.setRole(processActionReader.getRole());
		processActionType.setActionWorkflow(processActionReader.getActionWorkflow().getName());
		
		return processActionType;
	}
	
	private ActionStatusType getActionStatusTypeFromActionStatusReader(ActionStatusReader actionStatusReader) {
		ActionStatusType actionStatusType = new ActionStatusType();
		actionStatusType.setName(actionStatusReader.getActionName());
		actionStatusType.setIsTakenInCharge(actionStatusReader.isTakenInCharge());
		actionStatusType.setIsTerminated(actionStatusReader.isTerminated());
		
		Calendar terminationTime = actionStatusReader.getTerminationTime();
		if (terminationTime != null)
			actionStatusType.setTerminationTime(Resources.getXMLGregorianCalendar((GregorianCalendar) terminationTime));
		
		Actor actor = actionStatusReader.getActor();
		if (actor != null) {
			ActionStatusType.Actor newActor = new ActionStatusType.Actor();
			newActor.setName(actor.getName());
			newActor.setRole(actor.getRole());
			actionStatusType.setActor(newActor);
		}
		
		return actionStatusType;
	}
	
	
}
