package it.polito.dp2.WF.sol2;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import it.polito.dp2.WF.ActionStatusReader;
import it.polito.dp2.WF.ProcessReader;
import it.polito.dp2.WF.WorkflowReader;

public class Process implements ProcessReader {
	private Calendar startTime;
	private WorkflowReader workflow;
	private LinkedList<ActionStatus> actionsStatus;

	public Process(Calendar startTime, WorkflowReader workflow) {
		this.startTime = startTime;
		this.workflow = workflow;
		actionsStatus = new LinkedList<ActionStatus>();
	}

	@Override
	public Calendar getStartTime() {
		return startTime;
	}

	@Override
	public List<ActionStatusReader> getStatus() {
		return new ArrayList<ActionStatusReader>(actionsStatus);
	}

	@Override
	public WorkflowReader getWorkflow() {
		return workflow;
	}
	
	public void addActionStatus(ActionStatus actionStatus) {
		actionsStatus.add(actionStatus);
	}

}
