package it.polito.dp2.WF.sol2;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import it.polito.dp2.WF.Actor;
import it.polito.dp2.WF.WorkflowMonitorException;
import it.polito.dp2.WF.WorkflowReader;
import it.polito.dp2.WF.sol2.ProcessAction;
import it.polito.dp2.WF.sol2.Resources;
import it.polito.dp2.WF.sol2.Workflow;
import it.polito.dp2.WF.sol2.Action;
import it.polito.dp2.WF.sol2.SimpleAction;
import it.polito.dp2.WF.sol2.WorkflowMonitorFactory;
import it.polito.dp2.WF.sol2.WorkflowMonitorImplementation;
import it.polito.dp2.WF.sol2.jaxb.ActionStatusType;
import it.polito.dp2.WF.sol2.jaxb.ProcessActionType;
import it.polito.dp2.WF.sol2.jaxb.ProcessType;
import it.polito.dp2.WF.sol2.jaxb.SimpleActionType;
import it.polito.dp2.WF.sol2.jaxb.WorkflowMonitorType;
import it.polito.dp2.WF.sol2.jaxb.WorkflowType;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Logger;

public class WFInfoDeserializer {
	private static final int ENCLOSING_WORKFLOW_INDEX	= 0; 
	private static final int ACTION_WORKFLOW_INDEX		= 1; 
	private static final int PROCESS_ACTION_INDEX 		= 2;
	
	private static Logger logger = Logger.getLogger(WFInfoDeserializer.class.getName());
	
	private WorkflowMonitorFactory workflowMonitorFactory;
	private WorkflowMonitorImplementation monitor;
	private HashMap<String, ArrayList<String>> unsolvedNextActions;
	private LinkedList<String[]> unsolvedActionWorkflows;
	
	public WFInfoDeserializer() throws WorkflowMonitorException {
		Resources.setLogHandler(logger, true);
		try {
			workflowMonitorFactory = new WorkflowMonitorFactory();
			monitor = (WorkflowMonitorImplementation) workflowMonitorFactory.newWorkflowMonitor();
			unsolvedNextActions = new HashMap<String, ArrayList<String>>(); 
			unsolvedActionWorkflows = new LinkedList<String[]>();
		} catch (WorkflowMonitorException wme) {
			logger.severe(wme.getMessage());
			throw wme;
		}  catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		}
	}

	public WFInfoDeserializer(WorkflowMonitorImplementation monitor) throws WorkflowMonitorException {
		Resources.setLogHandler(logger, true);
		try {
			this.monitor = monitor;
			unsolvedNextActions = new HashMap<String, ArrayList<String>>();
			unsolvedActionWorkflows = new LinkedList<String[]>();
		}  catch (Exception e) {
			logger.severe(e.getMessage());
			throw new WorkflowMonitorException(e);
		}
	}
	
	public void deserialize(String fileName) throws WorkflowMonitorException {
		logger.info("Starting deserializer on file " + fileName + " ...");
		try {
			JAXBContext context = JAXBContext.newInstance(Resources.CONTEXT_PATH);
			
			SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File(Resources.SCHEMA_LOCATION));
			
			Unmarshaller unmarshaller = context.createUnmarshaller();
			unmarshaller.setSchema(schema);
			@SuppressWarnings("unchecked")
			JAXBElement<WorkflowMonitorType> element = (JAXBElement<WorkflowMonitorType>) unmarshaller.unmarshal(new FileInputStream(fileName));
			WorkflowMonitorType workflowMonitorType = element.getValue();
			
			List<WorkflowType> workflowTypes = workflowMonitorType.getWorkflow();
			List<ProcessType> processTypes = workflowMonitorType.getProcess();
			
			for (WorkflowType workflowType : workflowTypes)
				monitor.addWorkflow(getWorkflowFromWorkflowType(workflowType));
			
			solveActionWorkflows();
			
			for (ProcessType processType : processTypes)
				monitor.addProcess(getProcessFromProcessType(processType));
			
		} catch (WorkflowMonitorException e) {
			throw e;
		} catch (Exception e) {
			throw new WorkflowMonitorException(e);
		} finally {
			logger.info("Deserializer terminated");	
		}
	}
		
	public Workflow getWorkflowFromWorkflowType(WorkflowType workflowType) throws WorkflowMonitorException {
		String name = workflowType.getName();
		if (name == null || name.isEmpty())
			throw new WorkflowMonitorException("Workflow name attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid workflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		Workflow workflow = new Workflow(name);
		
		List<SimpleActionType> simpleActionTypes = workflowType.getSimpleAction();
		for (SimpleActionType simpleActionType : simpleActionTypes)
			workflow.addAction(getSimpleActionFromSimpleActionType(simpleActionType, workflow));
		
		List<ProcessActionType> processActionTypes = workflowType.getProcessAction();
		for (ProcessActionType processActionType : processActionTypes)
			workflow.addAction(getProcessActionFromProcessActionType(processActionType, workflow));
		
		for (Entry<String, ArrayList<String>> entry : unsolvedNextActions.entrySet()) {
	    	SimpleAction action = (SimpleAction) workflow.getAction(entry.getKey());
	    	ArrayList<String> nextActions = entry.getValue();
	    	for (String nextAction : nextActions) {
	    		if (!nextAction.matches(Resources.NAME_REGEX))
	    			throw new WorkflowMonitorException("Invalid nextAction name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
	    		Action a = (Action) workflow.getAction(nextAction);
	    		if (a == null)
	    			throw new WorkflowMonitorException("Invalid name for next action " + nextAction + ", action not exists.");
	    		
	    		action.addPossibleNextAction(a);
			}
	    }
		unsolvedNextActions.clear();
		
		return workflow;
	}
	
	public Process getProcessFromProcessType(ProcessType processType) throws WorkflowMonitorException {
		String workflowName = processType.getWorkflow();
		if (workflowName == null || workflowName.isEmpty())
			throw new WorkflowMonitorException("Process workflow attribute is null or empty.");
		if (!workflowName.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid workflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		Workflow workflow = (Workflow) monitor.getWorkflow(workflowName);
		if (workflow == null)
			throw new WorkflowMonitorException("Invalid workflow.");
		
		XMLGregorianCalendar calendar = processType.getStartTime();
		if (calendar == null || !calendar.isValid())
			throw new WorkflowMonitorException("Date time not present or invalid.");
		
		Process process = new Process(calendar.toGregorianCalendar(), workflow);
		
		List<ActionStatusType> actionStatusTypes = processType.getActionStatus();
		for (ActionStatusType actionStatusType : actionStatusTypes) {
			process.addActionStatus(getActionStatusFromActionStatusType(actionStatusType, process, workflow));
		}
		
		workflow.addProcess(process);
		
		return process;
	}
	
	public SimpleAction getSimpleActionFromSimpleActionType(SimpleActionType simpleActionType, WorkflowReader enclosingWorkflow) throws WorkflowMonitorException {
		String name = simpleActionType.getName();
		if (name == null || name.isEmpty())
			throw new WorkflowMonitorException("Action name attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid simpleAction name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");

		String role = simpleActionType.getRole();
		if (role == null || role.isEmpty())
			throw new WorkflowMonitorException("Action role attribute is null or empty.");
		if (!role.matches(Resources.ROLE_REGEX))
			throw new WorkflowMonitorException("Invalid role name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");

		String workflow = simpleActionType.getEnclosingWorkflow();
		if (workflow == null || workflow.isEmpty())
			throw new WorkflowMonitorException("Action enclosingWorkflow attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid action enclosingWorkflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		if (!workflow.equals(enclosingWorkflow.getName()))
			throw new WorkflowMonitorException("Invalid enclosingWorkflow.");
		
		SimpleAction simpleAction = new SimpleAction(name, role, enclosingWorkflow, simpleActionType.isIsAutomaticallyInstantiated());
		unsolvedNextActions.put(name, (ArrayList<String>) simpleActionType.getNextAction());
		
		return simpleAction;
	}
	
	public ProcessAction getProcessActionFromProcessActionType(ProcessActionType processActionType, WorkflowReader enclosingWorkflow) throws WorkflowMonitorException {
		String name = processActionType.getName();
		if (name == null || name.isEmpty())
			throw new WorkflowMonitorException("Action name attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid processAction name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		String role = processActionType.getRole();
		if (role == null || role.isEmpty())
			throw new WorkflowMonitorException("Action role attribute is null or empty.");
		if (!role.matches(Resources.ROLE_REGEX))
			throw new WorkflowMonitorException("Invalid role name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		String workflow = processActionType.getEnclosingWorkflow();
		if (workflow == null || workflow.isEmpty())
			throw new WorkflowMonitorException("Action enclosingWorkflow attribute is null or empty.");
		if (!name.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid action enclosingWorkflow name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		if (!workflow.equals(enclosingWorkflow.getName()))
			throw new WorkflowMonitorException("Invalid enclosingWorkflow.");
		
		Boolean ai = processActionType.isIsAutomaticallyInstantiated();
		boolean automaticallyInstantiated = (ai == null)? false : ai;  
		ProcessAction processAction = new ProcessAction(name, role, enclosingWorkflow, automaticallyInstantiated);
		
		String actionWorkflowName = processActionType.getActionWorkflow();
		if (actionWorkflowName == null || actionWorkflowName.isEmpty())
			throw new WorkflowMonitorException("Action actionWorkflowName attribute is null or empty.");
		if (!actionWorkflowName.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid actionWorkflowName name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		
		Workflow actionWorkflow = (Workflow) monitor.getWorkflow(actionWorkflowName);
		if (actionWorkflow != null) {
			processAction.setActionWorkflow(actionWorkflow);
		} else {
			unsolvedActionWorkflows.add(new String[] { enclosingWorkflow.getName(), actionWorkflowName, name });
		}
		
		return processAction;
	}
	
	public ActionStatus getActionStatusFromActionStatusType(ActionStatusType actionStatusType, Process process, Workflow workflow) throws WorkflowMonitorException {
		String actionName = actionStatusType.getName();
		if (actionName == null || actionName.isEmpty())
			throw new WorkflowMonitorException("ActionStatus name attribute is null or empty.");
		if (!actionName.matches(Resources.NAME_REGEX))
			throw new WorkflowMonitorException("Invalid actionStatus name format, it must be a string made of alphanumeric characters only, where the first character is alphabetic.");
		Action action = (Action) workflow.getAction(actionName);
		if (action == null)
			throw new WorkflowMonitorException("The corrispondent action in workflow not exists.");
		
		XMLGregorianCalendar calendar = actionStatusType.getTerminationTime();
		GregorianCalendar terminationTime = null;
		if (calendar != null) {
			if (!calendar.isValid())
				throw new WorkflowMonitorException("Date time not present or invalid.");
			
			terminationTime = calendar.toGregorianCalendar();
			if (terminationTime.before(process.getStartTime()))
				throw new WorkflowMonitorException("Invalid termination time.");			
		}
		
		it.polito.dp2.WF.sol2.jaxb.ActionStatusType.Actor a = actionStatusType.getActor();
		
		Actor actor = null;
		if (a != null) {
			String actorName = a.getName(), actorRole = a.getRole();
			if (actorName == null || actorRole == null)
				throw new WorkflowMonitorException("Actor name or role not present.");
			if (!actorName.matches(Resources.ROLE_REGEX) || !actorRole.matches(Resources.ROLE_REGEX))
				throw new WorkflowMonitorException("Invalid actor name format or role name format, they must be an alphabetic string.");
			if (!action.getRole().equals(actorRole))
		    	throw new WorkflowMonitorException("Invalid role value, expected " + action.getRole() + ".");

			actor = new Actor(actorName, actorRole);
		}
		
		boolean tc = (actionStatusType.isIsTakenInCharge() == null)? false : actionStatusType.isIsTakenInCharge().booleanValue();
		boolean t  = (actionStatusType.isIsTerminated() == null)? false : actionStatusType.isIsTerminated().booleanValue();
		if (tc == (actor == null))
	    	throw new WorkflowMonitorException("IsTakenInCharge and actor value are wrong.");
	    if (t == (terminationTime == null))
	    	throw new WorkflowMonitorException("IsTerminated and terminationTime value are wrong.");
	    if ((actor == null) && (terminationTime != null))
	    	throw new WorkflowMonitorException("terminationTime value and actor value are wrong.");
		
		return new ActionStatus(actionName, actor, terminationTime);
	}
	
	/**
	 * Method for set the correct action workflow reference to each process action.
	 * 
	 * @throws WorkflowMonitorException
	 */
	private void solveActionWorkflows() throws WorkflowMonitorException {
		logger.info("Solving all action Workflow...");
		
		for (String[] unsolved : unsolvedActionWorkflows) {
			Workflow actionWorkflow = (Workflow) monitor.getWorkflow(unsolved[ACTION_WORKFLOW_INDEX]);			
			if (actionWorkflow == null)
				throw new WorkflowMonitorException("Invalid " + Resources.ACTION_WORKFLOW + " name, it not exists.");
			
			Workflow enclosingWorkflow = (Workflow) monitor.getWorkflow(unsolved[ENCLOSING_WORKFLOW_INDEX]);
			ProcessAction processAction = (ProcessAction) enclosingWorkflow.getAction(unsolved[PROCESS_ACTION_INDEX]);
			processAction.setActionWorkflow(actionWorkflow);
		}
		unsolvedActionWorkflows.clear();
		
		logger.info("Action workflow solved");
	}	
}
