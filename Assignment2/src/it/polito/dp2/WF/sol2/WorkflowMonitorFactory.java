package it.polito.dp2.WF.sol2;

import it.polito.dp2.WF.WorkflowMonitor;
import it.polito.dp2.WF.WorkflowMonitorException;

public class WorkflowMonitorFactory extends it.polito.dp2.WF.WorkflowMonitorFactory {
	
	public WorkflowMonitorFactory() {
	}

	@Override
	public WorkflowMonitor newWorkflowMonitor() throws WorkflowMonitorException {
		return new WorkflowMonitorImplementation();
	}

}
